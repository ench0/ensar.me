const _ = require(`lodash`)
const Promise = require(`bluebird`)
const path = require(`path`)
const slash = require(`slash`)

const LodashModuleReplacementPlugin = require(`lodash-webpack-plugin`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  // return new Promise((resolve, reject) => {
  return new Promise((resolve) => {
    const blogPostTemplate = path.resolve(`src/templates/template-blog-post.js`)
    const tagPagesTemplate = path.resolve(`src/templates/template-tag-page.js`)
    const staticPagesTemplate = path.resolve(`src/templates/template-static-page.js`)

    /*
    ********************
      POSTS & TAGS
    ********************
    */
    graphql(
      `
        {
          allMarkdownRemark(
            limit: 1000
            sort: { fields: [frontmatter___date], order: DESC }
            filter: { frontmatter: { draft: { ne: true }, static: { ne: true } } }
          ) {
            edges {
              node {
                fields {
                  slug
                }
                frontmatter {
                  tags
                  title
                }
              }
            }
          }
        }
      `
    ).then((result) => {
      if (result.errors) {
        console.log(result.errors)
      }

      // Create blog posts pages.
      const posts = result.data.allMarkdownRemark.edges
      // console.log('POSTS', posts)
      posts.forEach(({ node }, index) => {
        createPage({
          path: node.fields.slug, // required
          component: slash(blogPostTemplate),
          context: {
            slug: node.fields.slug,
            highlight: node.frontmatter.highlight,
            shadow: node.frontmatter.shadow,
            next: index === 0 ? null : posts[index - 1].node,
            prev: index === posts.length - 1 ? null : posts[index + 1].node,
          },
        })
      })

      // Create tag pages.
      let tags = []
      result.data.allMarkdownRemark.edges.forEach((edge) => {
        if (_.get(edge, `node.frontmatter.tags`)) {
          tags = tags.concat(edge.node.frontmatter.tags)
        }
      })
      tags = _.uniq(tags)
      tags.forEach((tag) => {
        const tagPath = `/tags/${_.kebabCase(tag)}/`
        createPage({
          path: tagPath,
          component: tagPagesTemplate,
          context: {
            tag,
          },
        })
      })

      resolve()
    })

    /*
    ********************
      STATIC
    ********************
    */
    graphql(
      `
        {
          allMarkdownRemark(
            limit: 1000
            sort: { fields: [frontmatter___date], order: DESC }
            filter: { frontmatter: { draft: { ne: true }, static: { ne: false } } }
          ) {
            edges {
              node {
                fields {
                  slug
                }
                frontmatter {
                  title
                }
              }
            }
          }
        }
      `
    ).then((result) => {
      if (result.errors) {
        console.log(result.errors)
      }

      // Create static pages.
      const posts = result.data.allMarkdownRemark.edges
      // console.log('POSTS', posts)
      posts.forEach(({ node }, index) => {
        createPage({
          path: node.fields.slug, // required
          component: slash(staticPagesTemplate),
          context: {
            slug: node.fields.slug,
            highlight: node.frontmatter.highlight,
            shadow: node.frontmatter.shadow,
            next: index === 0 ? null : posts[index - 1].node,
            prev: index === posts.length - 1 ? null : posts[index + 1].node,
          },
        })
      })

      resolve()
    })
  })
}

// Add custom url pathname for blog posts.
exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `File`) {
    const parsedFilePath = path.parse(node.absolutePath)
    const slug = `/${parsedFilePath.dir.split(`---`)[1]}/`
    createNodeField({ node, name: `slug`, value: slug })
  } else if (node.internal.type === `MarkdownRemark` && typeof node.slug === `undefined`) {
    const fileNode = getNode(node.parent)
    createNodeField({
      node,
      name: `slug`,
      value: fileNode.fields.slug,
    })

    if (node.frontmatter.tags) {
      const tagSlugs = node.frontmatter.tags.map((tag) => `/tags/${_.kebabCase(tag)}/`)
      createNodeField({ node, name: `tagSlugs`, value: tagSlugs })
    }
  }
}

// Sass and Lodash.
exports.onCreateWebpackConfig = ({ stage, actions }) => {
  switch (stage) {
    case `build-javascript`:
      actions.setWebpackConfig({
        plugins: [new LodashModuleReplacementPlugin()],
      })
  }
}
