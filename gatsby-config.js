module.exports = {
  siteMetadata: {
    title: `ensar.me - PhD Blog Website`,
    author: `@ensar`,
    description: `PhD blog and other stories`,
    homepage: `https://ensar.me`,
    keywords: `phd, js, react, semantic web, linked data, privacy, gdpr`,
  },
  mapping: {
    'MarkdownRemark.frontmatter.author': `AuthorYaml`,
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: `pages`,
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/styles/typography.js`,
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        excerpt_separator: `<!-- end -->`,
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
              showCaptions: true,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-copy-linked-files`,
          {
            resolve: `gatsby-remark-smartypants`,
            options: {
              dashes: `oldschool`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-autolink-headers`,
          `gatsby-remark-katex`,
          {
            resolve: 'gatsby-remark-custom-blocks',
            options: {
              blocks: {
                excerpt: 'excerpt',
                info: 'info',
              },
            },
          },
          {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
              trackingId: 'UA-73320827-11',
              // Puts tracking script in the head instead of the body
              head: false,
              // Setting this parameter is optional
              anonymize: true,
              // Setting this parameter is also optional
              respectDNT: true,
              // Avoids sending pageview hits from custom paths
              exclude: ['/preview/**', '/do-not-track/me/too/'],
            },
          },
          // {
          //   resolve: `gatsby-plugin-favicon`,
          //   options: {
          //     logo: './src/pages/media/favicon.png',
          //     injectHTML: true,
          //     icons: {
          //       android: true,
          //       appleIcon: true,
          //       appleStartup: true,
          //       coast: false,
          //       favicons: true,
          //       firefox: true,
          //       twitter: true,
          //       yandex: false,
          //       windows: true,
          //     },
          //   },
          // },
          {
            resolve: `gatsby-plugin-manifest`,
            options: {
              name: 'Ensar.me',
              short_name: 'Ensar',
              start_url: '/',
              background_color: '#f7f0eb',
              theme_color: '#a2466c',
              display: 'minimal-ui',
              icon: 'src/pages/media/favicon.png', // This path is relative to the root of the site.
            },
          },
        ],
      },
    },
    `gatsby-transformer-yaml`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-catch-links`,
    `gatsby-plugin-glamor`,
  ],
}
