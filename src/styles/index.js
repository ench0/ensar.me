import { style, media, css } from 'glamor'
import bg from './img/bg-paper.jpg'
import { rhythm, scale } from './typography'
import styleColors from './colors'
import presets from './presets'

const { colors } = styleColors

const animations = {
  animationCurveFastOutSlowIn: `cubic-bezier(0.4, 0, 0.2, 1)`,
  animationCurveLinearOutSlowIn: `cubic-bezier(0, 0, 0.2, 1)`,
  animationCurveFastOutLinearIn: `cubic-bezier(0.4, 0, 1, 1)`,
  animationCurveDefault: `cubic-bezier(0.4, 0, 0.2, 1)`,
  animationSpeedDefault: `250ms`,
  animationSpeedFast: `200ms`,
  animationSpeedSlow: `300ms`,
}

// css.global('html, body', { padding: `10rem` })
css.global('header h1', { textAlign: `center`, marginBottom: rhythm(1 / 6) })
css.global('hr', {
  marginBottom: rhythm(0.5),
  marginTop: rhythm(1),
})

css.global('a', {
  borderColor: `${colors.link}`,
  color: `${colors.link}`,
  textDecoration: `none`,
  transition: `color .5s`,
  display: `inline-block`,
})

css.global('a:hover', {
  color: `${colors.black}`,
  // filter: `grayscale(75%) saturate(1) contrast(1.25) sepia(25%)`,
  transition: `color .5s`,
})

css.global('a:after', {
  color: `#ccc`,
  display: `block`,
  width: `100%`,
  margin: `-5px 0`,
  content: ` `,
  borderBottom: `solid 2px ${colors.linkHover}`,
  transform: `scaleX(0)`,
  transition: `transform 500ms ease-in-out`,
  transformOrigin: `0% 50%`,
})
css.global('a:hover:after', {
  transform: `scaleX(1)`,
  // transformOrigin: `100% 50%`,
})

export default {
  animations,
  colors,
  verticalPadding: style({
    padding: rhythm(3 / 4),
  }),
  container: style({
    // maxWidth: `37rem`,
    maxWidth: `64rem`,
    padding: `0.25rem`,
    margin: `0 auto`,
  }),
  main: style(
    {
      minWidth: `16rem`,
      margin: `0 .05rem .25rem`,
      padding: `2rem 1.5rem 1rem`,
      border: `1px solid #ddd`,
      boxShadow: `1px 1px 5px rgba(10, 10, 10, .25)`,
      background: `url(${bg})`,
      backgroundSize: `255px 180px`,
      textShadow: `1px 1px 1px rgba(10, 10, 10, .25)`,
    },
    media(presets.phablet, {
      margin: `0 1rem .5rem`,
      padding: `3rem 3rem 1rem`,
    }),
    media(presets.tablet, {
      margin: `0 1.5rem .75rem`,
      padding: `4.5rem 4.5rem 1rem`,
    }),
    media(presets.desktop, {
      margin: `0 2rem 1rem`,
      padding: `6rem 6rem 1rem`,
    })
  ),

  menu: style(
    {
      display: `block`,
      height: `1.5rem`,
      minWidth: `16rem`,
      margin: `.25rem .5rem 1rem .5rem`,

      padding: `0`,
      '& a': {
        fontSize: `90%`,
        color: colors.light,
      },
      '& a.active': {
        color: colors.black,
      },
      // border: `1px solid #ddd`,
      // boxShadow: `1px 1px 5px rgba(10, 10, 10, .25)`,
      // background: `url(${bg})`,
      // backgroundSize: `255px 180px`,
      // textShadow: `1px 1px 1px rgba(10, 10, 10, .25)`,
    },
    media(presets.phablet, {
      padding: `1rem 1rem 2rem`,
      '& a': {
        fontSize: `100%`,
      },
    }),
    media(presets.tablet, {
      padding: `1rem 1.5rem 2rem`,
      '& a': {
        fontSize: `110%`,
      },
    }),
    media(presets.desktop, {
      padding: `1rem 2rem 2rem`,
      '& a': {
        fontSize: `120%`,
      },
    })
  ),
  menuleft: style({
    float: `left`,
    ...scale(0.5),
    '& a': {
      display: `inline-block`,
      textDecoration: `none`,
      fontStyle: `italic`,
    },
  }),
  menuright: style({
    float: `right`,
    ...scale(0.5),
    '& a': {
      display: `inline-block`,
      textDecoration: `none`,
      fontStyle: `italic`,
      marginLeft: `1rem`,
    },
  }),

  link: style({
    // color: colors.light,
    transition: `color 1s text-shadow 1s`,

    display: `inline-block`,
    margin: 0,
    padding: 0,
    lineHeight: 1.3,

    ':hover': {
      color: colors.black,
      textShadow: `0px 0px 1px rgba(10, 10, 10, .25)`,
    },
    ':after': {
      display: `block`,
      width: `100%`,
      content: ` `,
      borderBottom: `solid 2px ${colors.linkHover}`,
      transform: `scaleX(0)`,
      transition: `transform 500ms ease-in-out`,
      transformOrigin: `0% 50%`,
    },
    ':hover:after': {
      transform: `scaleX(1)`,
    },
  }),

  abstract: style(
    {
      margin: `0rem`,
      display: `block`,
      lineHeight: `1.4`,
      ...scale(-0.5 / 5),
      color: `${colors.text}`,
      paddingBottom: `.25rem`,
      textAlign: `justify`,
      '& span': {
        fontStyle: `normal`,
        textAlign: `left`,
      },
    },
    media(presets.phablet, {
      margin: `0 2rem`,
    }),
    media(presets.tablet, {
      margin: `0 4rem`,
    })
  ),
  tags: style(
    {
      margin: `0rem`,
      display: `block`,
      lineHeight: `1.4`,
      ...scale(-0.5 / 5),
      color: `${colors.text}`,
      paddingBottom: `.25rem`,
      textAlign: `left`,
      '& span': {
        fontStyle: `normal`,
        textAlign: `left`,
      },
    },
    media(presets.phablet, {
      margin: `0 2rem`,
    }),
    media(presets.tablet, {
      margin: `0 4rem`,
    })
  ),

  cloud: style({
    color: colors.light,
    textAlign: `center`,
    listStyleType: `none`,
    margin: `3rem 0 2rem`,
    '& li': {
      display: `inline-block`,
      margin: `0 1rem`,
    },
  }),

  toc: style({
    margin: `1rem 0`,
    '& li': {
      marginBottom: `calc(1.2rem / 4)`,
    },
    '& h2': {
      // paddingTop: `1rem`,
    },
  }),

  mono: style({
    fontFamily: `"Space Mono",Consolas,"Roboto Mono","Droid Sans Mono","Liberation Mono",Menlo,Courier,monospace`,
    ...scale(-2 / 5),
    display: `block`,
    color: `${colors.light}`,
    paddingBottom: `.5rem`,
    textAlign: `center`,
  }),

  center: style({
    textAlign: `center`,
  }),

  justify: style({
    textAlign: `justify`,
  }),

  posts: style({
    marginBottom: rhythm(2),
    marginTop: rhythm(1),
    marginLeft: 0,
    listStyle: `none`,
    '& li': {
      clear: `both`,
      marginBottom: `0`,
      padding: `1.5rem 0`,
      borderBottom: `1px solid #ccc`,
      minHeight: `235px`,
    },
    '& img': {
      color: colors.light,
      width: `180px`,
      display: `block`,
      border: `1rem solid #efefef`,
      boxShadow: `1px 1px 3px rgba(10, 10, 10, .25)`,
      margin: `0 auto 1rem`,
      [presets.Tablet]: {
        float: `left`,
        marginRight: `1rem`,
      },
    },
    '& li span': {
      color: colors.light,
      display: `block`,
      [presets.Tablet]: {
        float: `right`,
        marginLeft: `1rem`,
      },
    },
  }),

  icon: style({
    marginBottom: rhythm(4 / 4),
    display: `flex`,
    alignItems: `center`,
    '& span': {
      color: colors.light,
      ...scale(-1 / 5),
    },
    '& small': {
      fontWeight: `bold`,
      color: colors.text,
      textTransform: `uppercase`,
    },
  }),

  prevNext: style({
    padding: `0`,
    margin: `0`,
    color: colors.light,
    textTransform: `uppercase`,
  }),

  text: style({
    marginTop: `1rem`,
    '& img': {
      filter: `grayscale(75%) saturate(1) contrast(1.25) sepia(25%)`,
      background: `#999`,
      border: `1rem solid #efefef`,
      boxShadow: `1px 1px 3px rgba(10, 10, 10, .25)`,
      transition: `filter .5s`,
    },
    '& img:hover': {
      filter: `grayscale(15%) saturate(1.5) contrast(1.5) sepia(15%)`,
    },
  }),
}
