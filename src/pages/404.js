import React from 'react'
import PropTypes from 'prop-types'

import 'katex/dist/katex.min.css'

import styles from '../styles'
import Layout from '../templates/layout'
import { scale } from '../styles/typography'

class FourZeroFourRoute extends React.Component {
  render() {
    const date = <div {...styles.mono}>{`Hmmm... Where is it?`}</div>

    return (
      <Layout location={this.props.location}>
        <div
          css={
            {
              // maxWidth: rhythm(26),
            }
          }
        >
          <header>
            <h1>Four-Zero-Four</h1>
            {date}
            <div {...styles.abstract} {...styles.center}>
              <strong>Abstract.</strong>
              {` The page does not exist. Or maybe it does but I'm hiding it from you.`}
            </div>
          </header>

          <div {...styles.text}>
            <h1
              css={{
                ...scale(20 / 5),
                color: `${styles.colors.text}`,
                paddingBottom: `1rem`,
                textAlign: `center`,
              }}
            >
              404
            </h1>
            <div
              css={{
                ...scale(1 / 5),
                color: `${styles.colors.light}`,
                paddingBottom: `1rem`,
                textAlign: `center`,
              }}
            >
              Nothing to see here
            </div>
          </div>
          <hr />
        </div>
      </Layout>
    )
  }
}

export default FourZeroFourRoute

FourZeroFourRoute.propTypes = {
  location: PropTypes.object,
}
