---
title: "Choosing the Right Web Host"
headline: "Web host guide - pick the right server for your website"
abstract: "This will be first in series of articles where we will try to build simple website from scratch, using NodeJS as a platform of choice."
date: "2017-10-18"
static: false
draft: false
author: Ensar H
indexImage: "719ebc40-14ec-4d0c-9c70-92f2e1e54db9.jpg"
tags:
  - article
  - vps
  - linode
  - digitalocean
  - vultr
  - webdev
  - hosting
---

[[excerpt]]
| In the series of articles that are hopefully ahead of us, we will try to build a simple website, from the very scratch. We will not be using ready-made solutions, but rather try to build it brick by brick. And in the process understand how things are made and what are the technologies behind the web as we know it. But first, a disclaimer. I am not claiming to be a world-class expert in the field and quite contrary, in some aspects I am but a beginner. But I will try to learn as I go and pass my common-sense knowledge that I gathered during years of working in IT industry at large. I imagine this as a two-way street, I will welcome your comments, suggestions and critique.

<!-- end -->

![](719ebc40-14ec-4d0c-9c70-92f2e1e54db9.jpg)

Before going deeper into reasons why should you pick one  specific programming language/platform for your next web project, we will examine the logistics. It is important to decide what server should be behind your website. There are definitely many shortcuts out there if you want to build your website, like one-click blog installations, content management systems and such that we will explore in other articles. And that is perfectly fine, if you just need a blog to write your stuff. But if you are one of those people who need to know how things are made, please continue.

Right here, I am going to advise you against using shared hosting providers. You end up sharing your website space/bandwidth with potentially hundreds or even thousands of other users. More often than not, if one of those users has issues on the website, like being hacked, running bad scripts, doing CPU or disk heavy operations, it will impact your website as well. Not very secure and reliable way to run your precious website.

What hosting companies are usually doing is overselling their space. That means that if they offer, for example, 1 TB of hard drive space and sell it to 1000 people, that would mean that the hosting computer would have capacity of 1000 TB, right? Wrong. They count on the fact that 99.9% of users would not be using even 1% of the space assigned to them. That way they can offer 1 TB space for 1000 users on 10 TB PC.

If you are one of those outliers and start using even 25% of the (promised) space, you will soon notice that they start throttling your speed or sending you emails of warning.

Off course, there are still some very honest and professional hosting companies and I had a pleasure to share-host my website with. But, to experience a real freedom and be in full control, you need your private server. You have 3 options:

1. Dedicated host/server\
2. Virtual Private Server (VPS)\
3. Managed VPS

First option is way too expensive, you rent your own server and pay the monthly bill. Reason behind the coat is the bandwidth and hard drive space are quite expensive. There is always an option to host the server yourself, even at home, but unless you are doing this for hobby and do not care about speed and up-time, do not do it yourself(tm).

Second option is the best for many reasons. Third option is good as well if you do not need too much freedom and want some aspects of the server to be managed by someone else. But then, you are loosing much in learning process and you are reading the wrong article.

So stick with option 2 and profit. Especially since in recent years the pricing of such servers went so significantly down that it is not even an issue anymore. There are 3 major players in this field and I will go from one to another in order of my experience in using them.

Linode is the grandpa of all the VPS providers. They are longest in the business and revolutionised the way we see VPS-es of modern days. I still remember buying my first Li(node) back in the days when it was \$20 per month for the cheapest (128 or 256MB of RAM, don't really remember). And the closest price of someone from the competing offers was little less than \$100. Along the way came other provider (mentioned in next paragraph) who took a significant chunk of their market just for doing things more elegantly and, well, being new kid on the block. Linode was left behind with their older XEN technology and mechanical hard drives powering their machines. Since then they *did* catch up and continue to be relevant with SSD powered servers and KLM technology.

Digital Ocean did everything right from the beginning. Great user interface (as opposed to Linode's old-fashioned one), modern approach to nodes, pardon 'droplets', strong community, aggressive pricing, fantastic reliability. It now has a majority of market share and it made the VPS servers much more interesting for the businesses world-wide. It offers user profiles/teams where you can set up a server with separate account for your clients where they can pay for the hosting services without going through you. That right there was the reason I moved a lot of my websites to DO.

Of other players in the market, I have recently tried Vultr and I am more than happy to use it for few of my websites. They offer same modern user interface as DO, but are even more aggressive on pricing. For example, I needed an independently hosted website with just a basic offering and they fit the bill more than enough: 512 MB server for $2.50 p/m! So far there were no performance issues and I am very pleased. Performance wise they seem to beat the competition by a solid margin, at least based on some [benchmarks](https://www.webstack.de/blog/e/cloud-hosting-provider-comparison-2017/).

Bottom line, for your website pick any of the 3 of the above and you can't go wrong. My current recommendation: for business and accounting flexibility go with Digital Ocean. For performance and price go with Vultr. But again, you will not go wrong with Linode, although it is not my favourite at this moment.

At the time of writing this article, Linode had 194,456 domains associated with their nameserver, Digital Ocean 410,776 and Vultr 32,798. That can probably give you a rough idea about their popularity. Information was available through [tcpiputils.com](https://www.tcpiputils.com).

From here, we will explore content management systems, web frameworks and more, so stay tuned.