---
title: "Ophelia"
headline: "Hurricane Ophelia"
abstract: "Ophelia is raging across Ireland at the  moment."
date: "2017-10-16"
static: false
draft: false
author: Ensar H
indexImage: "img-3762.jpg"
tags:
  - other
  - storm
  - hurricane
---

[[excerpt]]
| The strongest storm in last 50 years hit Ireland today.

<!-- end -->

![](img-3762.jpg)

There seems to be a lot of structural damage in its path, but place where I live escaped without any major consequences. Thank God! The winds are still not over, but the storm is expected to dissipate until tomorrow.