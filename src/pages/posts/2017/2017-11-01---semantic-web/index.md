---
title: "Semantic Web"
headline: "Semantic Web and Linked Data"
abstract: "This article goes over explaining the concepts of Semantic Web  and Linked Data in context of today's world wide web as we know it."
date: "2017-11-01"
static: false
draft: false
author: Ensar H
indexImage: 'nodes.png'
tags:
  - article
  - linked data
  - semantic web
  - www
  - phd
---

## World Wide Web
[[excerpt]]
| Ever since the creation of the network that we now know as the World Wide Web (WWW), or just ‘the Web’, there have been different approaches to store and manipulate data. Even at the onset of the Web, and with simultaneous advent of **HTML** (HyperText Markup Language), plain text could contain links and formatting rules. In such way different chunks of the Web, known as websites, could be connected through hyperlinks, referencing one document with another, both inside one website or between them. The document can be plain text, picture (displayed directly inside the browser and formatted if needed through its engine), or any generic file (downloadable through a link).

<!-- end -->

What happened at that point is that the data being processed became too big to be represented through the usage of flat/plain file formats such as CSV. The websites started separating the data from the files, using database management systems such as **SQL**, known as relational database. Although SQL as a concept pre-dates the Web (1974 for SQL, 1990 for WWW), it was not until mid-1990s that we witnessed emergence of MSSQL, MySQL, PostgreSQL. The websites powered by such technology of relational database language were able to use more sophisticated data manipulation, creating queries and document relations not possible by using plain, old file system only. This was possible through server side technologies and languages and frameworks such as Perl (CGI), PHP, Java, Ruby on Rails, Python frameworks and most recently Node.

However, the browsers still rendered HTML pages, using same **HTTP** (HyperText Transfer Protocol), with help of static **CSS** files for decoration and **Javascript** for effects and usability features. The problem was, that the amount of data was getting bigger every minute and second and it was confined to the website as a closed unit. Obvious outcome for companies and other entities was that querying and displaying data required much more resources in forms of hardware upgrades, server clusters, code optimisations; all in attempt to make this data more readily accessible in terms of speed and simplicity. There were attempts to make different approach to organise data and mini-revolution was born now known as **NoSQL** or document database. Although it was an old concept dating back to 1960’s, it became the new buzzword when so-called Big Data was born in the early 2000’s.  It focused on querying large sets of data, but could not effectively handle data relations as it was not its main purpose. It did make it easier for certain big companies to handle their own mega-data better, but did not solve issue of global data partitioning.

## Linked Data

The question at hand was how to interlink and relate that data as a whole, across the Web. That is when the concept of **Web of Data** (WOD) was born. The existing Web was made for human interaction, with nicely formatted websites, usability features, etc, and we can refer to it as the Web of Documents. As described above, those documents can be in different formats and (hyper)linked between each other.

![](nodes.png)

The Web of Data, on the other hand, was made to be readable by both humans and machines. The idea is to have some sort of ‘underground web’, not normally accessible through regular web browsers, but via different methods. When Web of Data is organised so it provides resources or datasets easily readable by the machines (by means of some form of **RDF** serialisation), it becomes what is known as the Semantic Web. At times, Web of Data and Semantic Web are used interchangeably by some authors, although the former term emerged more recently once it was realised that the immediate benefits of the concept are related to sharing of the data rather than the meaning or semantics. However, very important concept was more or less agreed upon, known as Linked Data (LD). Similarly as with WOD vs SW debate, there is no unified definition for LD. 

Sometimes it is only referred to as ‘Semantic Web done right’, but commonly accepted opinion is that the LD is basically a concept governing the Semantic Web. The idea is to have all of the data presented to follow certain paradigms and rules. Tim Berners-Lee formulated those rules in 2006, also known as Linked Data Principles. They are accepted by the community in general and are as follows: using **Uniform Resource Identifiers** (URIs) as names for things; use HTTP URIs so that people can look up those names; when someone looks up a URI, provide useful information, using the standards (RDF, SPARQL); include links to other URIs, so that they can discover more things.

In short, Linked data is the method or way to publish the data, through the use of URIs, HTTP and RDF, in a way that it can be further manipulated through the semantic queries. ‘Plain’ HTML links for example tell us that there is a relation between two documents. Sometimes the relation is obvious, as in photograph being linked to an article. Many other times it is not and that is where RDF (Resource Description Framework) comes into play. It can not just describe the relation between two objects, but also between data fragments, as well as the type or nature of that relation (typed links). 

The whole LD movement started in early 2007 with W3C Linking Open Data (LOD) project. Companies and organisations had seen the opportunity to participate in the project as a way to help themselves run their own business. For example, BBC was and remains very active in the project as the data they contribute makes their work easier. That way they link their own websites to the data found in the WOD making it much simpler to maintain the huge amount of data within. The whole network of the interlinked data exploded since the early days and keeps getting bigger and bigger with thousands major clusters of data in the WOD. Most prominent datasets today are governed by DBpedia and Geo Names, with links to almost all other smaller datasets. DBPedia is important hub that represents the information from Wikipedia in RDF format. As Wikipedia emerged to be the most comprehensive general encyclopaedia of everything and anything (setting aside actual scientific accuracy questioned by most academic sources), it becomes obvious that DBPedia can have similar role in ‘Linked Data World’. There is currently no single entity that is used as an index for all the Web of Data available. DBPedia in such case can be used as a starting point for expanding our search for the data in the wild.

## Vocabularies and URIs

RDF Vocabulary Definition Language (RDFS)  and the Web Ontology Language (OWL) are used for creating vocabularies of all the ‘things’ or objects and describe how they are related. Vocabularies can be described as collections of classes and properties which can be connected by RDF **triples** that link classes and properties in one vocabulary to those in another.

When it comes to URI usage patterns, there are two usually accepted ways to identify the object in the datasets: 303 URIs and hash URIs. Both patterns are serving one main purpose: to enable us to distinguish between URIs that refer to real objects (things, entities) and  Web documents describing those real-world objects.\
Linked Data in theory should be published together with several types of metadata, in order to increase its usability and reliability. It can contain the information about the creator, the creation date, history (provenance) etc.

To access all of the data in the Semantic Web, we have different servers, browsers and search engines designed for this very purpose. Still, there are many challenges to be addressed. The ultimate goal is known; it is to be able use the Web as the single global database. Probably the biggest challenge is adopting the standards by all the relevant parties to be used in every single dataset. That way querying the terms in vocabularies would be seamless, making such tasks as scientific research, innovation or commercial development much easier.
