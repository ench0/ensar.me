---
title: "Problem Solving Festival 2017"
headline: "Problem solving festival for all the family"
abstract: "The family event proved to be very successful, bringing 2,000 people to enjoy the family fun day."
date: "2017-10-01"
static: false
draft: false
author: Ensar H
indexImage: "img-6969.jpg"
tags:
  - volunteer
  - family
  - event
  - festival
  - problem solving
  - study
---

[[excerpt]]
| I Volunteered at “Problem Solving Festival”, organised by SFI in TCD, 30st September 2017. Day filled with events for all the family and interesting challenges for everyone to participate.

<!-- end -->

![](img-6969.jpg)

![](img-3754.jpg)

I enjoyed handing balloons to the kids and surveying both kids and parents. Great feedback.

![](img-9893.jpg)

![](img-7731.jpg)