---
title: "AI: Accelerating Impact"
headline: "AI: Accelerating Impact and Theme E Deep Dive"
abstract: "Important session on future of Artificial Intelligence, including 'Deep Dive' session of my Theme."
date: "2017-11-30"
static: false
draft: false
author: Ensar H
indexImage: "IMG_5556.jpg"
tags:
  - ai
  - croke park
  - presentations
  - study
---

[[excerpt]]
| Future of AI was on display in Croke Park today at AI: Accelerating Impact. Hosted by the Science Foundation Ireland funded ADAPT Centre, the half-day seminar marked 10 years for digital content expertise from the centre, and brought together world-leading researchers and industry executives to discuss this critical field from a variety of wide-ranging perspectives.

<!-- end -->

Enjoyable day at Croke Park, apart from the main event, featured poster presentations and various sessions.

![](IMG_5556.jpg)

## Deep Dive

One session of particular interest to me and my studies was 'Theme E deep dive', presenting our Theme's work to Scientific Advisory Board.

### Details

  Scientiﬁc Advisory Board – Theme E; Thursday 30 November 2017 14:30- 1700 Croke Park Conference Centre

  Theme E advisors: Andrew Bredenkamp, Francis Tsang


### Presenters

* Dave Lewis - GDPR Slide
* Judie Attard - Data Value 
* Alan Meehan - Interlink Validation
* Jeremy Debattista - Data Quality
* Kris McGlinn - Smart buildings