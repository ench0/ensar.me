---
title: "Other Stuff"
headline: "All the things unrelated to the thing"
abstract: "Together with tracking my PhD progress, which is the main purpose of this site/blog, I would be writing about my previous experience in all things IT."
date: "2017-06-11"
static: false
draft: false
author: Ensar H
indexImage: 'books.jpg'
tags:
  - other
  - off-topic
---

[[excerpt]]
| I have spent a good chunk of my life doing web design and development, graphic design, networking, sysadmin, troubleshooting and programming. I will write about stuff not directly related to anything in particular, but will provide you with advice coming from the years of experience,  mostly sticking with IT related stuff.

<!-- end -->

![Other stuff](books.jpg)

I built web servers from scratch, dealing with Linux, BSD, Apache, Nginx, Cherokee, PHP, more than dozen CMS-es, monitoring tools, security, firewall and more. I built SME servers ranging from pure Linux to specialised distros like Ebox (Zentyal), ClearOS, Windows Servers. I was working as graphic designer, technical officer, IT consultant and built database systems. And all of that is just from top of my head. :)