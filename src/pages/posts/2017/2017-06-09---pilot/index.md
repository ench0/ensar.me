---
title: "Pilot"
headline: "The pilot project"
abstract: "I am PhD candidate with the Adapt Centre at the Trinity College Dublin. My current area of research is around the Semantic Web, Linked Data and related technologies; more specifically legal side of it. I am investigating how the concepts of privacy, intellectual property and licensing influence that side of the web. My main supervisor is Dr. Dave Lewis."
date: "2017-06-09"
static: false
draft: false
author: Ensar H
indexImage: 'semantic-quest.png'
tags:
  - phd
  - linked data
  - semantic web
  - survey
---

## Dissecting the Data
[[excerpt]]
| Although there are many avenues to explore, and generally we can argue that there are specific needs in this area that could be tackled immediately, I decided to take somewhat unconventional, first-hand approach to the issue.
As I was never practically involved in gathering, analysing and presenting the data, I wanted come up with a way to practically be engaged in all of these steps. In my previous (and current somewhat) carrier as a web developer, many times I was given task to create web sites to serve different purposes. As the nature of business is dictated by the time vs money relation, very often I was forced to choose the ready-made solutions, rather than developing one of my own. My job would be to customise, tweak and adjust, scratching only the surface and understanding only smaller parts of the whole.
In a way that was frustrating and in conflict with my nature. I can not really appreciate or enjoy something unless I understand it inside and out. Even as a boy, I would take my toys apart, trying to understand how they actually work. Now, finally, I have opportunity that, as a part of my research, create an app that will collect data, analyse it and finally output in way that is in line with the standards of the Semantic Web.

<!-- end -->

## No More Shortcuts

I could have taken an easy approach, use Google forms or similar for example, and just convert the results to RDF-friendly output. But for the reasons mentioned above, I did all this from the scratch. That being said, I have used my previous extensive experience in web development to apply best security practices to the app itself.

![](semantic-quest.png)

That is why I created Questionnaire app, hosted [here](http://semantic.farend.net/). The website is running on Virtual Private Server (VPS), hosted by Digital Ocean in their London node. Operating system is Ubuntu 16.04 LTS, using built-in ufw command for IP tables (firewall rules). Language of choice is Elixir, running on Erlang's Beam, and powered by Phoenix Framework. For the styling, I have used Milligram CSS framework, with my own custom CSS3-based effects. Forms use Parsley JavaScript form validation jQuery plugin to check for the form requirements. Finally, for the database layer I have used PostgreSQL supporting both classic relational database and document database format, all wrapped in ultra secure storage implementation.

## Pilot Study

To actually start using the system, I have created a pilot study to better understand the needs of my research institution. It is pending ethical approval from the Ethics Committee.

## Open Source
The project is available under MIT license on my Github pages. It is fully transparent and open to expansion.

[1]: https://adaptcentre.ie
[2]: https://tcd.ie
