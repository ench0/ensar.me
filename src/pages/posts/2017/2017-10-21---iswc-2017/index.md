---
title: "ISWC 2017"
headline: "International Semantic Web Conference 2017"
abstract: "This year’s ISWC was taking place in Vienna. I have presented a paper at SemSci workshop."
date: "2017-10-21"
static: false
draft: false
author: Ensar H
indexImage: "5dc7abcc-ba87-4865-8511-c20c9518c86b.jpg"
tags:
  - conference
  - iswc
  - phd
  - workshop
  - paper
  - gdpr
  - decentralisation
  - blockchain
  - privacy
  - semantic web
  - linked data
  - vienna
  - austria
---

[[excerpt]]
| This year's ISWC is taking place in Vienna, Austria, 21st October 2017. Five days packed with events, with two workshop days leading to the main conference.

<!-- end -->

!['linked'](5dc7abcc-ba87-4865-8511-c20c9518c86b.jpg)

## Workshops

I presented my paper at **SemSci** workshop, first day of workshops and tutorials session and chaired by Daniel Garijo and Tobias Kuhn. Paper title was \"Linked Data Contracts to Support Data Protection and Data Ethics in the Sharing of Scientific Data\", available at the [workshop proceedings](http://ceur-ws.org/Vol-1931/).

I was trying to raise awareness of the potential issues in data sharing that are regulated by upcoming GDPR. It was enjoyable experience, with workshop proved to be very popular. My presentation was observed by approx. 60 people and based on great feedback and post-workshop requests for information and staying in touch, I would say it did have certain impact at the audience.

Next workshop, the following day, was **PrivOn** workshop chaired by Sabrina Kirrane and very related to the work that I am doing as well. Paper was presented by my supervisor prof. Dave Lewis, also authored by Kaniz Fatema, Harshvardhan Pandit and myself, entitled \"Compliance through Informed Consent: Semantic Based Consent Permission and Data Management Model\", dealing with consents in light of GDPR. My colleague Harsh presented another paper, again very relevant to my topic, but dealing with (pseudo)anonymisation and privacy in context of GDPR, entitled \"Modelling provenance for GDPR compliance using linked open data vocabularies\". The topics of Adapt's papers were spot-on, as there has been a lot of mentioning at other presentations about anonymisation in relation to privacy.

Another interesting workshop that same day was **DeSemWeb**: Decentralizing the Semantic Web, chaired by Ruben Verborgh, Andrei Sambra and Tobias Kuhn. I attended post-lunch session that was mainly about blockchain technology. The technology behind services such as Bitcoin was built with decentralisation in mind and therefore very relevant to the workshop's topic. From the workshop's organisers, Ruben was also present at the European counterpart of this conference where I presented our earlier paper; while Tobias was also a chair of the workshop where I presented my paper a day earlier. Blockchain is therefore something I should look into as it keeps popping up in my recent discussions. On the other hand, there was very heated discussion about the very definition and purpose of blockchain. Is it just another technology for applying distributed computing, and one of the implementations was distributed ledgers and provenance tracking, similar that others technologies provide, like Bittorrent for example? Or something else entirely?\
The workshop sparked a lot interest in the topic. A lot of familiar faces that I met at ESWC's workshop were here.

![](67ae996e-bf08-4307-bd8f-a84dff930ea3.jpg)

![](c6107a1b-27c3-42ab-8cf0-dc989ca43765.jpg)

## Main Conference

For the main conference there were some very interesting topics. Again there was a nice presentation on blockchain, but the highlight of the day was Daniel Gariho's presentation on Widoco, really useful tool for creating automated documentation files for the ontologies just by inputting RDF into the system. It had live demo of the system and quick explanation of the options. Something that I will most likely use for my DPRL ontology. 

Another interesting session was 'minute madness' where 60+ participants had only 40 seconds to introduce their idea and explain their poster/demo that would be shown later that evening in foyer of the main building. There were some really serious pitches including our own Kris' pitch on geolocation. Others were like watching standup comedians. Really funny and entertaining session.

Second day of the conference was quieter in a sense there were not that many top-tier sessions and the crowd was already smaller. Interesting session on social media though, and techniques of matching linked data profiles with social media profiles. Another enjoyable session was about hyperlinks and concept of 'Relevant relation assessment'.