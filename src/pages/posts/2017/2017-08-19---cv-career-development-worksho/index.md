---
title: "CV - Career Development Workshop"
headline: "How to write CV and advance your career"
abstract: "CV workshop for postdocs and research fellows"
date: "2017-08-19"
static: false
draft: false
author: Ensar H
indexImage: "001.jpg"
tags:
  - workshop
  - career
  - cv
  - study
---

[[excerpt]]
| CV workshop organised on 18th August 2017, led by Maire Brophy and Sinead Gorham.

<!-- end -->

Targeted towards postdocs and research fellows mostly, but still some usable insight for the rest of us.

![](001.jpg)