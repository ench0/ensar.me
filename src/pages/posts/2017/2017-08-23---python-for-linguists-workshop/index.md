---
title: "Python for Linguists Workshop"
headline: "Python for Linguists Workshop at DCU"
abstract: "Python for Linguists Workshop at DCU was engaging and intensive workshop."
date: "2017-08-23"
static: false
draft: false
author: Ensar H
indexImage: "agenda-python-session-i-copy.jpg"
tags:
  - phd
  - workshop
  - python
  - learning
---

[[excerpt]]
| Attended “Python for Linguists Workshop”, organised by Sheila Castilho, Adapt Centre DCU, with first session 21/22 August 2017.

<!-- end -->

It was 4-day intensive workshop delivered by Constantin Orasan. Interesting and engaging event was targeted towards researchers in linguistics related disciplines, but it was applicable for wider audience as well. We have learned starting from the basics, gradually moving towards more advanced topics.

Not directly related to my field, but still wanted to keep my options open for the rest of the studies.

![](agenda-python-session-i-copy.jpg)