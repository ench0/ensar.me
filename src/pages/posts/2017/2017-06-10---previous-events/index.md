---
title: "Previous events"
headline: "Events attended up until May 2017"
abstract: "I have attended number of events prior to starting this blog. This is a short list, for record purposes."
date: "2017-06-10"
static: false
draft: false
author: Ensar H
indexImage: "timeline.png"
tags:
  - volunteer
  - study
  - phd
  - event
  - seminar
  - workshop
  - symposium
---

[[excerpt]]
| Here's quick list of workshops and symposiums attended before May 2017 (when I started this blog), just to keep up to date. From now on, every event would have its own post.

<!-- end -->

![](timeline.png)

*\tAttended Linked Data workshop, organized by Christophe Debruyne, Adapt Centre in May 2016.\
*\tVolunteered in event organisation as well as attended “Privacy in the Digital Age: Is it Time for a New Paradigm?” seminar presented by Dr Jyn Schultze-Melling (Facebook Inc.) and Dr TJ McIntyre (UCD), organised by Adapt Centre, in July 2016.\
*\tAttended 2nd Annual Symposium at the Dublin Convention Centre, entitled “Rethinking the R of GRC and how we view Risk: Future Possibilities for Next Generation Enterprise Risk Management”, organised by GRCTC in December 2016.