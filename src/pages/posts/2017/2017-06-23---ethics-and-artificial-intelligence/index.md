---
title: "Ethics and Artificial Intelligence"
headline: "Ethics and Artificial Intelligence: Attitudes Survey 2017"
abstract: "As technologies develop and their applications become more extensive many ethical questions arise in terms of how these developments will affect human life into the future.  Of particular interest is how Artificial Intelligence (AI) and machine-learning will impact on human life in the areas of work, education, health, media, transport, and of course in the area of human relationships, intimacy and creativity."
date: "2017-06-23"
static: false
draft: false
author: Ensar H
indexImage: 'img1683.jpg'
tags:
  - volunteer
  - ethics
  - ai
  - survey
  - study
---

[[excerpt]]
| I was a volunteer at the Science Gallery on Wednesday, 3rd May 2017. The SG had very nice setup on AI theme, entitled \"Humans do not need apply\". The event was about awareness of the future filled with artificial intelligence and risks it might bring when it comes to employment of human beings. We are witnessing rapid increase in machines replacing human labour where ever technically possible.

<!-- end -->

![](img1683.jpg)

As a part of my involvement I was participating in gathering the participants for the survey organised by Dr Aphra Kerr from Maynooth University that was trying to raise awareness of the potential issues. I wish to thank everybody involved for invaluable experience that brought me, as I am in process of conducting another survey of my own.