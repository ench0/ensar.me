---
title: "Industry Partners Presentations"
headline: "Industry Partners Presentations 2017"
abstract: "Meeting with industry partners from around the world and also great opportunity to present our work."
date: "2017-12-01"
static: false
draft: false
author: Ensar H
indexImage: "IMG_4016.jpg"
tags:
  - study
  - industry
  - chartered accountants
  - presentations
---

[[excerpt]]
| Important week in Adapt, continuing on from yesterday's Scientific Advisory Board presentations, to also deliver few presentations to our industry partners. 

<!-- end -->

![](IMG_4016.jpg)

Sessions were held in Chartered Accountants House and featured presentation on GDPR by Dr. Dave Lewis.

![](IMG_8546.jpg)
