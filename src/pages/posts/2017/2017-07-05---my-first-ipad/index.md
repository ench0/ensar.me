---
title: "My First iPad"
headline: "iPad Mini makes a perfect reading device"
abstract: "While using my laptop is sufficient for most of my tasks related to the studies, there was still a small gap. As it becomes more and more apparent, I need to do a lot of reading if I want to drive my PhD to its final destination. iPad seems like a perfect fit."
date: "2017-07-05"
static: false
draft: false
author: Ensar H
indexImage: 'img-3566.jpg'
tags:
  - ipad
  - apple
  - gadgets
  - tech
  - study
---

[[excerpt]]
| I got it as a gift from my wife actually and so far I love it. It makes everything a little bit easier with programs that help me do PDF reading and annotating, like for example PDF Expert.

<!-- end -->

![](img-3566.jpg)

As I already have iPhone 6s (4.7 inch) I have chosen to go with smaller iPad - Mini 4. I guess having one of those\"Plus\" models would make one consider to go for larger variant of the device. For me, it is quite nice difference in size to justify the purchase (7.9 inch). And so far I haven't regretted it - it is light and just about the right size. As for the features, I am pleasantly surprised when it comes to general usability. Everything works better on bigger screen and many of my regular apps have iPad optimised versions - adding even more features to them. And now living dangerously and running iOS 11 first beta, it feels even better.

I was a bit reluctant to get the older model vs one of those new 'Pro' models. But when I recently tried wife's Pro, it felt just too big and heavy for regular use and now I am genuinely happy with the purchase. Might be my best investment in technology in last couple of years.