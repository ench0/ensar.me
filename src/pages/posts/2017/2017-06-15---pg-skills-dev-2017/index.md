---
title: "PG Skills Dev 2017"
headline: "Postgraduate Skills Development: Summer School 2017"
abstract: "On 15th June I have attended Postgraduate Skills Development Summer School organised by Student Learning Development at TCD. There were several sessions spanned across two days, both morning and afternoon."
date: "2017-06-15"
static: false
draft: false
author: Ensar H
indexImage: 'photo-on-15-06-2017-at-12-33.jpg'
tags:
  - school
  - phd
  - personal development
  - time management
---

[[excerpt]]
| Excerpt

<!-- end -->

The one that was particularly interesting to myself was about time management, by Dr. Tamara O'Connor. Originally I wanted to attend Endnote training session that was happening at the same time in different room, but got interested in this one instead. Reason is that lately I have issues organising my free time and using it fully to benefit my studies. Actually even my non-free time is not perfectly balanced and fully utilised. Ironically, I wanted to attend all the sessions today, but due to the poor time management I only attended the one about time management.

![](photo-on-15-06-2017-at-12-33.jpg)

Planning - monitoring - evaluating are three key concepts in relation to managing our time. Keeping timeline plan is very important aspect, as we need to have set deadlines for our mid-term goals as well as to stay focused on our final goal.

![](photo-on-15-06-2017-at-11-41.jpg)

Gannt chart, roadblocks, realistic and measurable goals, sleep deprivation, concentration, distraction, procrastination are some of the concepts discussed in the session. It was very interactive as well, incorporating small exercises and exchanging information both ways. Overall, very informative presentation and I hope this will have longer impact on my future studies. And, of course, that I will find the time to follow the rules (pun intended).