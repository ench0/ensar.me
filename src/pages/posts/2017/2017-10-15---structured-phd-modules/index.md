---
title: "Structured PhD Modules"
headline: "Structured PhD modules: Statistics and Research Methods"
abstract: "I enjoyed both of the modules and finished them successfully."
date: "2017-10-15"
static: false
draft: false
author: Ensar H
indexImage: "research-methods-poster.jpg"
tags:
  - phd
  - structured phd
---

[[excerpt]]
| During autumn and winter of 2016, I have attended lectures in two modules – Statistics for Research Students by Donal Martin and Research Methods by Prof. Khurshid Ahmad, which I have consequently successfully completed by sitting the examination and poster presentation.

<!-- end -->

Statistics was more or less maths with some theory and concepts to memorise.

Research Methods was more interactive as there was a poster presentation to be done at the end of a module. That was a mockup project that we have done in groups and it was interesting experience.

![](research-methods-poster.jpg)