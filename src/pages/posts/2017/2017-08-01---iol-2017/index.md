---
title: "IOL 2017"
headline: "2017 International Linguistics Olympiad"
abstract: "International Linguistics Olympiad was well organised event with participants from all over the world."
date: "2017-08-01"
static: false
draft: false
author: Ensar H
indexImage: "img-9092.jpg"
tags:
  - volunteer
  - linguistics
  - olympiad
  - dcu
  - study
---

[[excerpt]]
| I volunteered for “2017 International Linguistics Olympiad”, on initiative from Laura Grehan, Adapt Centre DCU on 31st July 2017.

<!-- end -->

![](img-9092.jpg)

It was pleasure to work with DCU members of Adapt that I have met for the first time, as I am placed in TCD.

![](img-7227.jpg)