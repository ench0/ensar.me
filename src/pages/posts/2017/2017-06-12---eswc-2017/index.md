---
title: "ESWC 2017"
headline: "European Semantic Web Conference 2017"
abstract: "I have attended 14th European Semantic Web Conference, in Portoroz, Slovenia and presented a paper \"Utilising Semantic Web Ontologies to Publish Experimental Workflows\", on one of the workshops. My first conference ever was experience better than I could imagine."
date: "2017-06-12"
static: false
draft: false
author: Ensar H
indexImage: 'img2313.jpg'
tags:
  - conference 
  - eswc
  - phd
  - workshop
  - paper
  - semantic web
  - linked data
---

[[excerpt]]
| 14th European Semantic Web Conference was held in beautiful town of Portoroz, Slovenia. The main Conference ran for 3 days, 30th May to 1st June, with several workshops organised two days ahead of the Conference. I presented my paper at the workshop entitled \"Enabling Decentralised Scholarly Communication\", organised by Sarven Capadisli, Amy Guy and David De Roure. The paper that I was presenting - \"Utilising Semantic Web Ontologies to Publish Experimental Workflows\" - was joint effort of my colleague Harsh (who was the main author), myself and our supervisor Dave.

<!-- end -->

![](img2313.jpg)

This was the very first time I presented a paper in a conference or a workshop. All in all, although very nervous in anticipation of my presentation, everything went very smoothly and much better than I expected. It felt great being one of the authors at such an important event in the field of Semantic Web.

![](img3246.jpg)

It was great opportunity to meet with the experts in the field, to get better insight of how relevant my research is and determine where to head next. The Conference as a whole was tremendous learning experience, and as it was my first, **it could be a defining point** in pursuing the rest of my studies.

![](img3213.jpg)

Plus, the weather, the town of Portoroz and surrounding areas were highly enjoyable. Nice people, food and atmosphere in and around the Conference. I have also met with the people behind the venue and they were tremendously helpful and kind (as I was travelling with family and they went out of their way to accommodate all our needs). Big thanks to Marija.

More info available at [Linked Research website](https://linkedresearch.org/events/eswc2017/).

[ESWC 2017](http://2017.eswc-conferences.org/)

[Full transcript](eswc2017-transcript.txt)