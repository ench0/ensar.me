---
title: "Adapt Startup Day – Accelerating Ideas"
headline: "Adapt Startup Day - Dragon's Den Live"
abstract: "Successful event organised in DCU, tackling commercialisation ideas. Featuring real-life \"Dragon's Den\""
date: "2017-09-22"
static: false
draft: false
author: Ensar H
indexImage: "img-2198.jpg"
tags:
  - study
  - seminar
  - sfi
  - dcu
---

[[excerpt]]
| I attended “Adapt Startup Day – Accelerating Ideas”, organised by SFI, DCU, on 21st September 2017.

<!-- end -->

Interesting event was all about bringing your research project ideas to life through commercialisation. Probably most interesting part of the afternoon was when 3 researchers pitched their ideas in front of 4 Irish entrepreneurs.

![](img-0949.jpg)

![](img-2198.jpg)

![](img-1663.jpg)

![](img-8772.jpg)

![](002.jpg)