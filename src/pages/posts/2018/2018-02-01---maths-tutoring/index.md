---
title: "Maths Tutoring"
headline: "CS First Year Maths Tutoring and Demonstrating"
abstract: "My first experience as a tutor/demonstrator for undergraduate students."
date: "2018-02-01"
static: false
draft: false
author: Ensar H
indexImage: "maths.jpg"
tags:
  - tutoring
  - demonstrating
  - maths
  - study
---

[[excerpt]]
| My role is to be available for once-a-week Maths tutorials and to help with assignments marking.

<!-- end -->

![](maths.jpg)

Maths was my passion that dates back to elementary school. Applying logic and deduction in order to solve various problems is something that brings you great satisfaction. Off course, if you are able to come to a solution, otherwise it is a plain nightmare. :)


