---
title: "W3C Workshop in Vienna"
headline: "W3C Workshop on Privacy and Linked Data"
abstract: "Workshop hosted by WU Vienna was an excellent opportunity to meet like-minded experts in field of Linked Data and privacy."
date: "2018-04-19"
static: false
draft: false
author: Ensar H
indexImage: "IMG_20180417_092828.jpg"
tags:
  - workshop
  - vienna
  - austria
  - w3c
  - privacy
  - linked data
  - phd
  - gdpr
---

[[excerpt]]
| The workshop happened on 17–18 April 2018, in WU Vienna, Austria. Two days of intensive sessions were packed with interesting content.

<!-- end -->

![](IMG_20180417_092828.jpg)

I also delivered 'position paper' in form of quick presentation to the audience. It was in relation to GDPR and my current area of interest, ODRL profile. Several interesting presentations, with Sabrina Kiranne's Special project delivery that has many commonalities to the work I am doing.

![](IMG_20180418_093127.jpg)

Later, we formed a [panel](https://www.w3.org/2018/vocabws/#schedule) where the audience was to ask questions specific to the workshop's topic.

Second day ended with survey/poll, where we would vote on topic that needs most attention when it comes to standardisation and privacy. Interesting experience and memorable event.

![](IMG_20180418_093546.jpg)
