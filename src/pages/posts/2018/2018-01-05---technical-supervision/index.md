---
title: "Technical Supervisor"
headline: "Technical Supervision for MSc Course"
abstract: "I was asked to be to take a role of Technical Supervisor in MSc Innovation module CS7CS2."
date: "2018-01-05"
static: false
draft: false
author: Ensar H
indexImage: "cradle.png"
tags:
  - study
  - msc
  - innovation
  - supervision
---

[[excerpt]]
| Completely new experience for me, pushing innovative business idea from canvas to implementation.

<!-- end -->

![](cradle.png)

Over the last few months I was assigned to group 'K', supervising 4 students that were supposed to produce an innovative idea, market it and investigate potential business opportunities. The project 'Automatic Cradle' was interesting and engaging and it was my pleasure to take the technical advisory role. We had several meetings arranged over couple of months that the course was running.
