---
title: "Transfer Report"
headline: "Transfer Report - First Half of the PhD"
abstract: "Over the last few months I have been working hard on producing Transfer Report, a culmination of my work to date."
date: "2018-06-01"
static: false
draft: false
author: Ensar H
indexImage: "Transfer Report - Background.png"
tags:
  - transfer report
  - phd
  - presentation
---

[[excerpt]]
| In pursue of confirmation of the work done to date, I have presented my proposal/plan for the rest of the studies. Prof. Declan O'Sullivan and Prof. Owen Conlan were in the panel. I received excellent feedback and even better recommendations where to channel my future efforts.

<!-- end -->

![](Transfer Report - Background.png)

Transfer Report is a result of months of writing, researching the state of the art on the subject, literature review and more. It was great experience and in a way small preview into what is expected for the actual dissertation. Credits go to my main supervisor Dr. Dave Lewis for his tireless efforts, guidance and patience with my work. Big thanks also to my (new) co-supervisor Dr. David Filip, for jumping in when needed the most and helping me to better understand the issues surrounding the PhD.

![](Transfer Report - Methodology.png)

Working title of my PhD is 'Data Sharing Agreements to Enhance Data Transactions Between Data Controllers in Response to GDPR'.


Now onto the rest of my studies. :_