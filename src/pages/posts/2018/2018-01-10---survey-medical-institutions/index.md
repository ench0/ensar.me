---
title: "Survey: Medical Institutions"
headline: "European Medical Institutions Focusing on Rare Diseases: Survey"
abstract: "The task at hand was to survey 30+ websites belonging to various European medical institutions."
date: "2018-01-10"
static: false
draft: false
author: Ensar H
indexImage: "medical.jpg"
tags:
  - medical
  - gdpr
  - privacy
  - phd
---

[[excerpt]]
| Main goal of the survey was to address how the institutions tackle privacy policy in their work and their websites. 

<!-- end -->

Main
![](medical.jpg)

Also, one of the goals of the survey is to discover and analyse ways the actual data is shared, without going too deep into methodology and data protection. I have collected the data/facts into a Google document, shared with Adapt colleagues. Most of the organisations were EU funded non-profits that have very limited  amount data on their websites, especially public-facing.

It was also good opportunity to understand what kind of scope I am looking for when it comes to my PhD thesis.