---
title: "About"
headline: "About me and this website"
abstract: "I am a PhD candidate with the Adapt Centre at Trinity College Dublin. My current area of research is Semantic Web, Linked Data and related technologies; more specifically legal side of it. I am investigating how the concepts of privacy, intellectual property and licensing influence that side of the web. My main supervisor is Dr. Dave Lewis."
date: "2017-10-12"
static: true
draft: false
author: Ensar H
tags:
  - about
---

[[excerpt]]
| Although there are many avenues to explore, and generally we can argue that there are specific needs in this area that could be tackled immediately, I decided to take somewhat unconventional, first-hand approach to the issue.

<!-- end -->

## Dissecting the Data

As I was never *practically* involved in gathering, analysing and presenting the data, I wanted to have first-hand experience and be really engaged in all of these steps. In my previous (and current somewhat) carrier as a web developer, many times I was given task to create web sites to serve different purposes. As business is usually dictated by the time vs money relation, very often I was forced to choose ready-made solutions rather than developing one of my own. My job would be to customise, tweak and adjust, scratching only the surface and understanding only smaller parts of the whole.

In a way that was frustrating and in conflict with my curious nature. I can not really appreciate or enjoy something unless I understand it inside and out. Even as a boy, I would take my toys apart, trying to understand how they actually work. Now, finally, I have opportunity that, as a part of my research, create an app that will collect data, analyse it and finally output in way that is in line with the standards of the Semantic Web. **No More Shortcuts**.

I could have taken an easy approach, used Google forms or similar system and then find a way to convert the results to a needed format. But for the reasons mentioned above, I did all this from the scratch. That being said, I have used my previous extensive experience in web development to apply best security and workflow practices to the app itself.

## Pilot Study

To actually start using the app and put it to the real-world test, I have created a pilot study to better understand the needs of my research institution. It is pending ethical approval from the Ethics Committee.

## Open Source

This project is available under MIT license on my Github pages. It is fully transparent and open to expansion.

## More About Me

I have been involved in IT related business for as long as I can remember. Starting with early home computers in mid-eighties, I have developed a lifetime passion for everything related to computers. I have used those early home computers to do my secondary school’s work designing brochures and other course material for other students to use, and to make simple but at that time impressive software solutions for solving specific mathematical problems. At that stage I have been very interested in using computers as a design tool and graphic editing, which came very useful in certain jobs that I have been doing over the course of my carrier. Later in my life and carrier I have discovered new fields of the IT, especially those related to networking and the early Internet. I have helped educational institutes to build early computer networks, achieving enormous experience in the process. Being involved in education related IT structure from my early professional carrier, and coming from family of academics myself, had indisputable effect on my future life and development.

I have witnessed the Internet changing its shape and rules, becoming dynamic sea of countless websites and pages. I have seen technologies come and go, new ones emerging on the legacies of the past, servers, languages, scripts, changing and evolving at rapid pace. My main passion emerged: it is building, developing, improving and securing the dynamic websites and everything else related to that field.

In my recent professional years I have focused on building websites completely from scratch, which gives me control over all of the individual components in the chain of development. I would build a server, using Linux operating system most of the time, install needed frameworks and applications, set up the security aspects of the system, and finally build a dynamic website on top of that. The process would continue by the ongoing maintenance, security and penetrability tests, speed optimisations via different caching and proxy methods. I have learned about many technologies in the field, web servers, proxy/cache servers, server side languages, client side scripts, firewall and IP tables, and emerging new technologies. I would pay extra attention to little details, in constant pursue of the perfection. When needed, I would use my programming skills to build custom elements for the website, like dynamic timetables, event schedules and similar.

Working with various clients, varying from small businesses to large organisations, I have offered all-in-one services catering for all their needs, from basic IT infrastructure to complicated websites and Intranet. I have learned how to build networks, computers systems, develop them and protect via custom made firewall appliances. I discovered best security practices when it comes to maintaining the network.

Apart from running my private business, I have been employed by the Trinity College Dublin, on permanent, part-time basis. Working with students and academic staff, I have developed an understanding of the inner structure and work of the educational system. I have built a reputation as reliable and very technically minded person, always willing to assist in various issues that pertain the IT in this part of the College. I have delivered several presentations to postgraduate doctoral students that concern the field of encryption and data protection, advising on best practices available today. I have been in charge of security and encryption of the computers used by the HSE to store confidential patient data, being discreet and trustworthy.

In the process I have developed an additional interest on ‘dissecting’ the various software components, log files, even the file systems. That is why I enthusiastically engaged in doing Masters degree in computer forensics at UCD, which I have finished with First Class Honours. What helped me the most in achieving that kind of result is my huge motivation and interest in the field. In the process I have discovered another passion: love for research and developing myself even further. I have become interested in the field of forensic and analysis not just from professional perspective, but from the academic side as well.

For that reason, data collection, analysis and integration would be next logical step in my pursue of expanding my skills and knowledge. I find this course excellent opportunity and myself being enthusiastic about researching and studying all over again. I appreciate the opportunity and I am also sure I can benefit the project and be an important asset to it, based on many sets of skills I have developed over my professional carrier. I live and breathe the IT, and my extracurricular activities, apart from being engaged with my closest family, are strongly related to IT, for instance helping developing open source projects. My current passion is mobile and front-end development. I keep myself always up to date in the whole IT and technology field, both on software and hardware front, keeping a close eye on latest research and development. I am also involved in voluntary and community work to the best of my abilities.

## How

This website is running on Virtual Private Server (VPS), hosted by ~~Digital Ocean in their London node~~ Vultr's New Jersey node. Operating system is Ubuntu 16.04 LTS, using built-in ufw command for IP tables (firewall rules). Language of choice is ~~Elixir~~ JavaScript, running on ~~Erlang's Beam~~ **NodeJS**, and powered by ~~Phoenix Framework~~ **Koa 2**. For the styling, I have used ~~Milligram~~ **UIKit** CSS framework, with my own custom CSS3-based effects. Forms use ~~Parsley JavaScript form validation jQuery plugin~~ CSRF tokens to check for the form ~~requirements~~ security. Finally, for the database layer I have used ~~PostgreSQL~~ ~~supporting both classic relational database and document database format~~ **MongoDB**, all wrapped in ultra secure storage implementation.

My journey of Elixir, Phoenix, NodeJS and Express/Koa is available under [programming](/tags/programming) tag.

Blog ToDo list:
* ~~NodeJS + Koa 2~~
* ~~Routing~~
* ~~Pug~~
* ~~CSRF tokens for forms~~
* ~~Image and file uploads / multipart forms~~
* ~~Image resize via Sharp~~
* ~~NoSQL/Mongo~~
* ~~MD Editor + rendering~~
* ~~Authentication~~
* ~~Git update~~
* ~~Response time~~
* Image description
* Voting
* Comments
* Contact form
* BCrypt for passwords
* Dynamic menu
* Blog database name and port in settings file (multiple blogs)
* SASS
* Webpack 
* Better error handling
* Tests
* Cookie consent"

## Where

### Trinity College Dublin

At Trinity College Dublin, the University of Dublin we provide a liberal environment where independence of thought is highly valued and all are encouraged to achieve their potential. We promote a diverse, interdisciplinary, inclusive environment which nurtures ground-breaking research, innovation, and creativity through engaging with issues of global significance. 

Located in a beautiful campus in the heart of Dublin’s city centre, Trinity is Ireland’s highest ranked university and one of the world’s top 100.  It is home to 17,000 undergraduate and postgraduate students across all the major disciplines in the arts and humanities, and in business, law, engineering, science, and health sciences.

Trinity’s tradition of independent intellectual inquiry has produced some of the world’s finest, most original minds including the writers Oscar Wilde and Samuel Beckett (Nobel laureate), the scientists William Rowan Hamilton and Ernest Walton (Nobel laureate), the political thinker Edmund Burke, and the former President of Ireland and UNHCR Mary Robinson. This tradition finds expression today in a campus culture of scholarship, innovation, creativity, entrepreneurship and dedication to societal reform.

### ADAPT Centre

The ADAPT research centre focuses on developing next generation digital technologies that transform how people communicate by helping to analyse, personalise and deliver digital data more effectively for businesses and individuals. ADAPT researchers are based in four leading universities: Trinity College Dublin, Dublin City University, University College Dublin and Dublin Institute of Technology. ADAPT's transformative tools allow you explore video, text,speech and image data in a natural way across languages and devices, helping companies unlock opportunities that exist within digital content to re-imagine how to connect people, process and data to realise new economic value.
