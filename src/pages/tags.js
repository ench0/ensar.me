import React from 'react'
import PropTypes from 'prop-types'

import { Link, graphql } from 'gatsby'
import kebabCase from 'lodash/kebabCase'
import Layout from '../templates/layout'
// import { rhythm } from '../styles/typography'
import styles from '../styles'

class TagsPageRoute extends React.Component {
  render() {
    const allTags = this.props.data.allMarkdownRemark.group

    // console.log(this.props.data.allMarkdownRemark)

    // get all fieldValue
    // function getFieldValue() {
    //   return allTags.map((d) => d.fieldValue)
    // }
    /* get all totalCount */
    function getTotalCount() {
      return allTags.map((d) => d.totalCount)
    }
    /*  get lowest no */
    // function getMinCount() {
    //   return Math.min(...getTotalCount())
    // }
    /* get highest no */
    function getMaxCount() {
      return Math.max(...getTotalCount())
    }

    // console.log(getFieldValue())
    // console.log(getTotalCount())
    // console.log(getMinCount())
    // console.log(getMaxCount())

    const maxCount = getMaxCount()

    const date = <div {...styles.mono}>{`Tag cloud`}</div>

    return (
      <Layout location={this.props.location}>
        <header>
          <h1>Tags</h1>
          {date}
          <div {...styles.abstract} {...styles.center}>
            <strong>Abstract.</strong> List of tags sorted alphabetically.
          </div>
        </header>
        <ul {...styles.cloud}>
          {allTags.map((tag) => (
            <li key={tag.fieldValue}>
              <Link
                style={{
                  textDecoration: `none`,
                  fontSize: `${(tag.totalCount * 200) / maxCount + 75}%`,
                }}
                to={`/tags/${kebabCase(tag.fieldValue)}/`}
              >
                {tag.fieldValue} (
                {tag.totalCount}
                )
              </Link>
            </li>
          ))}
        </ul>
      </Layout>
    )
  }
}

export default TagsPageRoute

export const pageQuery = graphql`
  query TagsQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(limit: 2000, filter: { frontmatter: { draft: { ne: true }, static: { ne: true } } }) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`
TagsPageRoute.propTypes = {
  location: PropTypes.object,
  data: PropTypes.object,
  children: PropTypes.object,
}
