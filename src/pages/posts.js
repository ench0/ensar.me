import React from 'react'
import PropTypes from 'prop-types'

import { Link, graphql } from 'gatsby'
import Layout from '../templates/layout'
import styles from '../styles'
// import presets from '../styles/presets'
// import { rhythm, scale } from '../styles/typography'
// import { rhythm } from '../styles/typography'

class Posts extends React.Component {
  render() {
    const posts = this.props.data.allMarkdownRemark.edges
    const postList = posts.map((post) => (
      <li key={post.node.fields.slug}>
        {post.node.frontmatter.indexImage ? (
          <img src={post.node.frontmatter.indexImage.childImageSharp.resolutions.src} alt="" />
        ) : (
          <img src={this.props.data.defaultImage.childImageSharp.sizes.src} alt="" />
        )}
        <span>{post.node.frontmatter.date}</span>
        <Link to={post.node.fields.slug}>
          <h2>{post.node.frontmatter.title}</h2>
          {
            // console.log(this.props.data.defaultImage.childImageSharp.sizes.src)
          }
        </Link>
        <div>
          <em>{post.node.frontmatter.headline}</em>
        </div>
        <div {...styles.justify}>
          {post.node.frontmatter.abstract}
          {'   '}
          <Link to={post.node.fields.slug}>Read more...</Link>
        </div>
      </li>
    ))

    const date = <div {...styles.mono}>{`Total posts: ${posts.length}`}</div>

    return (
      <Layout location={this.props.location}>
        <div>
          <header>
            <h1>Posts</h1>
            {date}
            <div {...styles.abstract} {...styles.center}>
              <strong>Abstract.</strong> List of posts sorted by date.
            </div>
          </header>
          <ul {...styles.posts}>{postList}</ul>
        </div>
      </Layout>
    )
  }
}

export default Posts

export const pageQuery = graphql`
  query PostsQuery {
    defaultImage: file(relativePath: { eq: "media/ensar.jpg" }) {
      childImageSharp {
        sizes(maxWidth: 180, maxHeight: 180) {
          src
        }
      }
    }
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { draft: { ne: true }, static: { ne: true } } }
    ) {
      edges {
        node {
          fields {
            slug
          }
          excerpt(pruneLength: 140)

          frontmatter {
            title
            headline
            abstract
            date(formatString: "DD MMM, YYYY")
            indexImage {
              childImageSharp {
                resolutions(width: 180, height: 180) {
                  width
                  height
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
`

Posts.propTypes = {
  location: PropTypes.object,
  data: PropTypes.object,
}
