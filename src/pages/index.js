import React from 'react'
import PropTypes from 'prop-types'

import 'katex/dist/katex.min.css'
import { Link, graphql } from 'gatsby'
import _ from 'lodash'

import styles from '../styles'
import Layout from '../templates/layout'

// const _ = require(`lodash`)
// import { rhythm, scale } from '../styles/typography'

class IndexRoute extends React.Component {
  render() {
    const date = <div {...styles.mono}>My Study Blog and Other Stories</div>
    const abstract = `Main purpose of this website is to keep track of my PhD and college life as it
    happens. It will also deal with programming, Big Data, tech in general and more.`

    const allTags = this.props.data.allMarkdownRemark.group
    allTags.sort((a, b) => b.totalCount - a.totalCount)

    // console.log(allTags)

    return (
      <Layout location={this.props.location}>
        <div
          css={
            {
              // maxWidth: rhythm(26),
            }
          }
        >
          <header>
            <h1>{`Ensar's PhD Blog`}</h1>
            {date}
            <div {...styles.abstract}>
              <strong>Abstract.</strong> {abstract}
            </div>
            <div {...styles.tags}>
              <strong>Keywords:</strong>{' '}
              <Link to={`/tags/${_.kebabCase(allTags[0].fieldValue)}`}>{allTags[0].fieldValue}</Link>,{' '}
              <Link to={`/tags/${_.kebabCase(allTags[1].fieldValue)}`}>{allTags[1].fieldValue}</Link>,{' '}
              <Link to={`/tags/${_.kebabCase(allTags[2].fieldValue)}`}>{allTags[2].fieldValue}</Link>,{' '}
              <Link to={`/tags/${_.kebabCase(allTags[3].fieldValue)}`}>{allTags[3].fieldValue}</Link>,{' '}
              <Link to={`/tags/${_.kebabCase(allTags[4].fieldValue)}`}>{allTags[4].fieldValue}</Link>
            </div>
          </header>

          <p {...styles.text} {...styles.justify}>
            First, welcome to this website! In year 2016 I have started PhD in Computer Science at Trinity College
            Dublin. I will use this blog to post about my progress, things I learned, as well as other topics I feel
            like writing about. Content is organised by using various tags; full list of topics in for of tag cloud is
            available under <Link to="/tags/">tags</Link> menu item. List of all available posts is under{' '}
            <Link to="/posts/">posts</Link>.
          </p>
          <h2>About</h2>
          <p {...styles.text} {...styles.justify}>
            To learn more about me, about the College and Adapt Centre; about this site and tech behind it go to{' '}
            <Link to="/about/">about</Link> page.
          </p>
          <h2>PhD Activities</h2>
          <p {...styles.text} {...styles.justify}>
            To have a look at the posts directly relevant to my PhD progress, go to <Link to="/tags/phd/">phd</Link>{' '}
            tag. As PhD is not just about writing papers and participating in conferences and workshops, I include my
            other PhD activities in <Link to="/tags/study/">study</Link> tag. That includes volunteering, joining
            various sessions and meetups, etc; all other things relevant to my college life at large.
          </p>
          <h2>Other</h2>
          <p {...styles.text} {...styles.justify}>
            I will occasionally write an <Link to="/tags/article/">article</Link> about the technology, programming,
            JavaScript and my other activities not related to study. Feel free to explore all other tags.
          </p>
          <hr />
        </div>
      </Layout>
    )
  }
}

export const pageQuery = graphql`
  query TopTagsQuery {
    allMarkdownRemark(
      limit: 500
      sort: { fields: [frontmatter___tags], order: DESC }
      filter: { frontmatter: { draft: { ne: true }, static: { ne: true } } }
    ) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`

export default IndexRoute

IndexRoute.propTypes = {
  location: PropTypes.object,
  data: PropTypes.object,
}
