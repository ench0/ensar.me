import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import Helmet from 'react-helmet'

import favicon57 from '../pages/media/favicons/apple-icon-57x57.png'
import favicon60 from '../pages/media/favicons/apple-icon-60x60.png'
import favicon72 from '../pages/media/favicons/apple-icon-72x72.png'
import favicon76 from '../pages/media/favicons/apple-icon-76x76.png'
import favicon114 from '../pages/media/favicons/apple-icon-114x114.png'
import favicon120 from '../pages/media/favicons/apple-icon-120x120.png'
import favicon144 from '../pages/media/favicons/apple-icon-144x144.png'
import favicon152 from '../pages/media/favicons/apple-icon-152x152.png'
import favicon180 from '../pages/media/favicons/apple-icon-180x180.png'

import favicon192 from '../pages/media/favicons/android-icon-192x192.png'

import favicon144ms from '../pages/media/favicons/ms-icon-144x144.png'

import favicon16 from '../pages/media/favicons/favicon-16x16.png'
import favicon32 from '../pages/media/favicons/favicon-32x32.png'
import favicon96 from '../pages/media/favicons/favicon-96x96.png'

// const manifest = new File([''], '../pages/media/favicons/manifest.json')
// import manifest from '../pages/media/favicons/manifest.json'
// console.log(manifest)

class Favicon extends React.Component {
  render() {
    return (
      <StaticQuery
        query={graphql`
          query HeadQuery {
            site {
              siteMetadata {
                author
                homepage
                description
                title
                keywords
              }
            }
          }
        `}
        render={(data) => (
          <div>
            <Helmet
              title={data.site.siteMetadata.title}
              meta={[
                { name: 'description', content: data.site.siteMetadata.description },
                { name: 'keywords', content: data.site.siteMetadata.keywords },
                { name: 'author', content: data.site.siteMetadata.author },
                // ms-icons
                { name: 'msapplication-TileColor', content: '#ffffff' },
                { name: 'msapplication-TileImage', content: `${favicon144ms}` },
                // { name: 'theme-color', content: '#ffffff' },//already defined by webmanifest
              ]}
              link={[
                { rel: 'shortcut icon', type: 'image/png', href: `${favicon96}` },
                // apple
                { rel: 'apple-touch-icon', sizes: '57x57', href: `${favicon57}` },
                { rel: 'apple-touch-icon', sizes: '60x60', href: `${favicon60}` },
                { rel: 'apple-touch-icon', sizes: '72x72', href: `${favicon72}` },
                { rel: 'apple-touch-icon', sizes: '76x76', href: `${favicon76}` },
                { rel: 'apple-touch-icon', sizes: '114x114', href: `${favicon114}` },
                { rel: 'apple-touch-icon', sizes: '120x120', href: `${favicon120}` },
                { rel: 'apple-touch-icon', sizes: '144x144', href: `${favicon144}` },
                { rel: 'apple-touch-icon', sizes: '152x152', href: `${favicon152}` },
                { rel: 'apple-touch-icon', sizes: '180x180', href: `${favicon180}` },
                // android
                { rel: 'icon', type: 'image/png', sizes: '192x192', href: `${favicon192}` },
                // other
                { rel: 'icon', type: 'image/png', sizes: '16x16', href: `${favicon16}` },
                { rel: 'icon', type: 'image/png', sizes: '32x32', href: `${favicon32}` },
                { rel: 'icon', type: 'image/png', sizes: '96x96', href: `${favicon96}` },
                // manifest
                // { rel: 'manifest', href: `${manifest}` },
              ]}
            />
          </div>
        )}
      />
    )
  }
}

export default Favicon
