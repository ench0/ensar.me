import React from 'react'
import PropTypes from 'prop-types'

import 'typeface-space-mono'
import 'typeface-spectral'

import 'prismjs/themes/prism-solarizedlight.css'

import { Link, StaticQuery, graphql } from 'gatsby'
import { scale } from '../styles/typography'
import styles from '../styles'

import Head from './head'

class Layout extends React.Component {
  render() {
    return (
      <StaticQuery
        query={graphql`
          query LayoutIndexQuery {
            site {
              siteMetadata {
                author
                homepage
                description
                title
                keywords
              }
            }
          }
        `}
        render={(data) => {
          const { author, homepage } = data.site.siteMetadata
          // console.log(data.site.siteMetadata)
          return (
            <div>
              <Head />
              <div {...styles.container} {...styles.verticalPadding}>
                <div {...styles.menu}>
                  <div {...styles.menuleft}>
                    <Link {...styles.link} to="/">
                      ensar.me
                    </Link>
                  </div>
                  <div {...styles.menuright}>
                    <Link {...styles.link} to="/about/" activeClassName="active">
                      about
                    </Link>
                    <Link {...styles.link} to="/posts/" activeClassName="active">
                      posts
                    </Link>
                    <Link {...styles.link} to="/tags/" activeClassName="active">
                      tags
                    </Link>
                  </div>
                </div>
              </div>

              <div {...styles.container} {...styles.verticalPadding}>
                <div {...styles.main}>
                  {this.props.children}
                  <div
                    css={{
                      ...scale(-0.5),
                      color: styles.colors.light,
                      textAlign: `center`,
                    }}
                  >
                    powered by
                    {` `}
                    <a target="_blank" rel="noopener noreferrer" href={homepage}>
                      {author}
                    </a>{' '}
                    using{' '}
                    <a href="https://reactjs.org/" target="_blank" rel="noopener noreferrer">
                      React
                    </a>{' '}
                    &{' '}
                    <a href="https://www.gatsbyjs.org/" target="_blank" rel="noopener noreferrer">
                      Gatsby
                    </a>
                  </div>
                </div>
              </div>
            </div>
          )
        }}
      />
    )
  }
}

export default Layout

Layout.propTypes = {
  children: PropTypes.any,
}
