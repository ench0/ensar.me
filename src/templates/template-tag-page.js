import React from 'react'
import PropTypes from 'prop-types'

import { Link, graphql } from 'gatsby'
import Layout from './layout'
import styles from '../styles'
// import presets from '../styles/presets'
// import { rhythm } from '../styles/typography'

class TagRoute extends React.Component {
  render() {
    const posts = this.props.data.allMarkdownRemark.edges

    // const postLinks = posts.map((post) => (
    //   <li key={post.node.fields.slug}>
    //     <Link to={post.node.fields.slug}>{post.node.frontmatter.title}</Link>
    //   </li>
    // ))

    const postList = posts.map((post) => (
      <li key={post.node.fields.slug}>
        {post.node.frontmatter.indexImage ? (
          <img src={post.node.frontmatter.indexImage.childImageSharp.resolutions.src} alt="" />
        ) : (
          <img src={this.props.data.defaultImage.childImageSharp.sizes.src} alt="" />
        )}
        <span>{post.node.frontmatter.date}</span>
        <Link to={post.node.fields.slug}>
          <h2>{post.node.frontmatter.title}</h2>
          {
            // console.log(this.props.data.defaultImage.childImageSharp.sizes.src)
          }
        </Link>
        <div>
          <em>{post.node.frontmatter.headline}</em>
        </div>
        <div {...styles.justify}>
          {post.node.frontmatter.abstract}
          {'   '}
          <Link to={post.node.fields.slug}>Read more...</Link>
        </div>
      </li>
    ))

    let postLabel
    this.props.data.allMarkdownRemark.totalCount === 1 ? (postLabel = 'post') : (postLabel = 'posts')

    const date = (
      <div {...styles.mono}>
        {'Total: '}
        {this.props.data.allMarkdownRemark.totalCount}
        {` `}
        {postLabel}
      </div>
    )

    return (
      <Layout location={this.props.location}>
        <header>
          <h1>Tag: {this.props.pageContext.tag}</h1>
          {date}
          <div {...styles.abstract} {...styles.center}>
            <strong>Abstract.</strong> List of posts tagged with “
            {this.props.pageContext.tag}
            ”, sorted by date.
          </div>
        </header>
        <ul {...styles.posts}>{postList}</ul>
        <p>
          <Link to="/tags/">Browse all tags</Link>
        </p>
      </Layout>
    )
  }
}

export default TagRoute

export const pageQuery = graphql`
  query TagPage($tag: String) {
    defaultImage: file(relativePath: { eq: "media/ensar.jpg" }) {
      childImageSharp {
        sizes(maxWidth: 180, maxHeight: 180) {
          src
        }
      }
    }
    allMarkdownRemark(
      limit: 1000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] }, draft: { ne: true }, static: { ne: true } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            headline
            abstract
            date(formatString: "DD MMM, YYYY")
            indexImage {
              childImageSharp {
                resolutions(width: 180, height: 180) {
                  width
                  height
                  src
                  srcSet
                }
              }
            }
          }
        }
      }
    }
  }
`

TagRoute.propTypes = {
  location: PropTypes.object,
  data: PropTypes.object,
  pageContext: PropTypes.object,
}
