import React from 'react'
import PropTypes from 'prop-types'

import { Link, graphql } from 'gatsby'
import Img from 'gatsby-image'
import RehypeReact from 'rehype-react'

import 'katex/dist/katex.min.css'

import styles from '../styles'
import Counter from '../components/Counter'
import Layout from './layout'
import { rhythm } from '../styles/typography'

const renderAst = new RehypeReact({
  createElement: React.createElement,
  components: { 'interactive-counter': Counter },
}).Compiler

class StaticPageRoute extends React.Component {
  render() {
    const post = this.props.data.markdownRemark
    let prev
    let next
    this.props.pageContext.prev
      ? (prev = (
          <p {...styles.prevNext}>
            Previous post:{' '}
            <Link to={this.props.pageContext.prev.fields.slug}>{this.props.pageContext.prev.frontmatter.title}</Link>
          </p>
        ))
      : (prev = '')
    this.props.pageContext.next
      ? (next = (
          <p {...styles.prevNext}>
            Next post:{' '}
            <Link to={this.props.pageContext.next.fields.slug}>{this.props.pageContext.next.frontmatter.title}</Link>
          </p>
        ))
      : (next = '')
    // const next = this.props.pageContext.next
    // const prev = this.props.pageContext.prev
    // console.log(next)
    // console.log(prev)

    // DATE
    const date = <div {...styles.mono}>{` `}</div>

    // ABSTRACT
    const abstract = `${post.frontmatter.abstract} | ${post.timeToRead} min read`

    // TOC
    let toc
    if (post.tableOfContents) {
      toc = (
        <div {...styles.toc}>
          <h2>Content</h2>
          <div dangerouslySetInnerHTML={{ __html: post.tableOfContents }} />
        </div>
      )
    } else {
      toc = ''
    }

    return (
      <Layout location={this.props.location}>
        <header>
          <h1>{post.frontmatter.headline}</h1>
          {date}
          <div {...styles.abstract}>
            <strong>Abstract.</strong> {abstract}
          </div>
        </header>
        {toc}
        <div {...styles.text}>{renderAst(post.htmlAst)}</div>
        <hr />
        <p {...styles.icon}>
          <Img
            alt={`Icon of ${post.frontmatter.author.id}`}
            fixed={post.frontmatter.author.icon.children[0].fixed}
            css={{
              borderRadius: `100%`,
              float: `left`,
              marginRight: rhythm(3 / 4),
              marginBottom: 0,
            }}
            Tag="span"
          />
          <span>
            <small>{post.frontmatter.author.id}</small>
            {` `}
            {post.frontmatter.author.bio}
          </span>
        </p>
        {/* prev and next */}
        {prev}
        {next}
      </Layout>
    )
  }
}

export default StaticPageRoute

export const pageQuery = graphql`
  query StaticPageBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      htmlAst
      timeToRead
      tableOfContents
      fields {
        tagSlugs
      }
      frontmatter {
        title
        headline
        abstract
        static
        date(formatString: "ddd, DD/MM/YYYY")
        author {
          id
          bio
          icon {
            children {
              ... on ImageSharp {
                fixed(width: 50, height: 50, quality: 75, grayscale: true) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    }
  }
`
StaticPageRoute.propTypes = {
  location: PropTypes.object,
  data: PropTypes.object,
  pageContext: PropTypes.object,
}
